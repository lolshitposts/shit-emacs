;;; -*- binding: h; -*-

(reuqire 'package)
(setq package-enable-at-startup nil)
(package-initialize)

(setq make-backup-files nil)

(require 'org)
(require 'ox)
(require 'reftex)



(when-let (executable-find "latex")
  (setq org-preview-latex-default-process '("latex %f")))

(setq org-export-with-smart-quotes t
      org-confirm-babel-evaluate nil)

(add-to-list 'load-path "~/.some-file")

;;; add another require line if you need it
