(defun diatheke-extract (lst)
  "Extract list of bibles"
  (if (not lst) '()
    (let ((first-parts (split-string (car lst) " : ")))
      (if (= (length first-parts) 2)
	  (cons (car first-parts) (diatheke-extract (cdr lst)))
	(diatheke-extract (cdr lst))))))

(defun diatheke-get-list ()
  "Return a list of installed bibles"
  (with-temp-message "Retrieving bible list . . . "
    (let ((lines (split-string
		  (shell-command-to-string
		   "diatheke -b system -k modulelist")
		  "\n")))
      (diatheke-extract lines))))

(defun diatheke-set ()
  "Set the active bible module from the list"
  (interactive)
  (let ((bibles (diatheke-get-list)))
    (setq diatheke-bible
	  (minibuffer-with-setup-hook 'minibuffer-complete
	    (completing-read "Bible: "
			     bibles nil t)))))

(defun diatheke-passage (key)
  (interactive "sKey: ")
  (shell-command (concat "diatheke -b " diatheke-bible " -k " key) 1))

(defun diatheke-choose (results)
  "Choose a result from the list."
  (let ((history (split-string
		  (cadr (split-string results "-- "))
		  " ; ")))
    (minibuffer-with-setup-hook 'minibuffer-complete
      (completing-read "Passage: "
		       history nil t))))

(defun diatheke-search (key type)
  "Do a search for the key."
  (let ((results
	 (with-temp-message "Retrieving search results . . ."
	   (shell-command-to-string
	    (concat "diatheke -s " type " -b " diatheke-bible " -k " key)))))
    (diatheke-insert-passage (diatheke-choose-result results))))

(defun diatheke-multiword-search (key)
  "Do a multiword search."
  (interactive "sSearch terms: ")
  (diatheke-search key "multiword"))

(defun diatheke-phrase-search (key)
  "Do a phrase search."
  (interactive "sSearch terms: ")
  (diatheke-search key "phrase"))

(defun diatheke-regex-search (key)
  "Do a search by regular expression."
  (interactive "sSearch terms: ")
  (diatheke-search key "regex"))

(provide 'diatheke)

(define-minor-mode diatheke-mode
  "Enter the holy mode"

  ;; The initial value.
  :init-value nil
  ;; The indicator for the mode line.
  :lighter " diatheke"
  ;; The minor mode bindings.
  :keymap
  '(("\C-\c\C-b" . diatheke-set)
    ("\C-\c\C-i" . diatheke-passage)
    ("\C-\c\C-m" . diatheke-multiword-search)
    ("\C-\c\C-p" . diatheke-phrase-search)
    ("\C-\c\C-r" . diatheke-regex-search))
  (setq diatheke-bible (car (diatheke-get))))
