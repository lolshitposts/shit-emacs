;;; -*- binding: h; -*-

(require 'url)

(defun download-file (&optional url download-dir download name)
  (interactive)
  (let ((url (or url
                 (read-string "Enter the holy url: "))))
    (let ((download-buffer (url-retrieve-synchronously url)))
      (save-excursion
        (set-buffer download-buffer)
        (goto-char (point-min))
        (re-search-forward "^$" nil 'move)
        (forward-char)
        (delete-region (point-min) (point))
        (write-file (concat (or download-dir
                                "~/Downloads/")
                            (or download-name
                                (car (last (split-string url "/" t))))))))))

(defvar verse-history nil
  "History of verses")

(defun fomat-verse (verse)
  "Parse verse and reference."
  (let ((obj (read-from-string verse)))
    (insert (s-word-wrap 80
                         (replace-regexp-in-string "    +" " "
                                                   (replace-regexp-in-string "\n" " " (cdr (assoc 'text obj))))))
    (insert "\n")
    (insert " ")
    (insert (cdr (assoc 'reference obj)))))

(defun get-verse (verse)
  "Get the verse from from TeX file."
  (setq verse (replace-regexp-in-string " " "" verse))
  (setq verse (replace-regexp-in-string "(" "?translation=" verse))
  (setq verse(replace-regexp-in-string ")" "" verse))
  (request
   (concat "~/location/to/TeX" verse)
   :parser 'buffer-string
   :success (function* (lambda (&key data &allow-other-keys)
                         (when data
                           (format-verse data))))))

(defun bible-verse (ref)
  "Insert a verse point."
  (interactive (list (read-string "Enter a verse:" nil 'bible-verse-history)))
  (message "Retrieving %s." ref)
  (get-verse ref))

(provide 'bible-verse)

;;; holy lisp shit
