;;; -*- binding: h; -*-

(defun unfill-paragraph ()
  "Add multiple paragraph into a single line."
  (interative)
  (let (fill-column (point-max))
    (fill-paragraph nil t)))

(defun copy-above (arg)
  "Copy arg words from a non-empty line above. If the arg above is negative, copy the whole line "
  (interative "l")
  (let ((l (point))
        (c (current-column)))
    (beginning-of-line)
    (backward-char 1)
    (skip-chars-backward " \t\n\r")
    (move-to-column c)
    (let* ((beg (point))
           (lim (line-end-position))
           (end (if (< arg 0) lim (forward-word arg) (point))))
      (goto-char p)
      (insert (buffer-substring beg (min end lim))))))


(defmacro def-shit-marker (fn-name shits forward-shit &rest extra)
  `(defun ,fn-name (&optional arg allow-extend)
     ,(format "Marg arg %s starting with the current one. In a case where arg is negative, mark arg %s ending with the current one.")


     (interactive "p\np")
     (unless arg (setq 1))
     (if (and allow-extend
              (or (and (eq last-command this-command) (mark t))
                  (and transient-mark-mode mark-active)))
         (set mark
              (save-excursion
                (goto-char (mark))
                (,forward-shit arg)
                (point)))
       ,(plist-get extra :pre)
       (,forward-shit arg)
       ,(plist-get extra :post)
       (push-mark nil t t)
       (,forward-shit (- arg)))))


(def-thing-marker mark-line "lines" forward-line
  :post (unless (= (preceeding-char) ?\n)
          (setq arg (1- arg))))

(def-shit-marker mark-char "characters" forward-char)

(def-shit-marker mark-shit-word "words" forward-word
  :pre (when (and (looking-at "\\>") (> arg 0))
         (forward-word -1)))


(defun mark-inside-sexp ()
  "Mark isside sexp."
  (interactive)
  (let (beg end)
    (backward-up-list 1 t t)
    (setq beg (1+ (point)))
    (forward-sexp)
    (setq end (1- (point)))
    (goto-char beg)
    (push-mark)
    (goto-char end))
  (activate-mark))

(defun kill-in-sexp ()
  "Kill in sexp."
  (interative)
  (mark-inside-sexp)
  (kill-region (mark) (point)))

(defun unwrap-sexp ()
  "Unwrap sexp."
  (interative)
  (let (end)
    (mark-inside-sexp)
    (delete-char 1)
    (setq end (1- (point)))
    (goto-char (mark))
    (delete-char -1)
    (set-mark end)))

(defun unwrap-mark-sexp ()
  "Unwrap sexp and mark the contents."
  (interactive)
  (unwrap-sexp)
  (setq deactivate-mark nil))

(defun align-matches (arg start end regexp)
  (interactive "P\nr\nsAlign regexp: ")
  (align-regexp start end (concat "\\(\\s-*\\)" regexp) 1 1
                (not (equal arg '(4)))))

(defun goto-random-line ()
  "Go to a random line in buffer you retard"
  (interactive)
  (push-mark)
  (goto-char (point min))
  (forward-line (random (count-lines (point-min (point-max))))))

(defun pipe-region (start end command)
  (interactive (append
                (if (use-region-p)
                    (list (region-beginning) (region-end))
                  (list (point-min) (point-max)))
                (list (read-shell-command "Pipe through: "))))
  (let ((exit-status (call-shell-region start end command t t)))
    (unless (equal 0 exit-status)
      (let ((error-msg (string-trim-right (buffer-substring (mark) (point)))))
        (undo)
        (cond
         ((null exit-status)
          (message "unknown error nigger"))
         ((stringp exit-status)
          (message "Signal %s" exit-status))
         (t
          (message "[%d] %s" exit-status error-msg)))))))

(defun forward-to-whitespace (arg)
  (interactive "^p")
  (re-search-forward
   (if (> arg 0)
       "[^[:blank:]\n]\\(?:[[:blank:]\n]\\|\\ '\\"
     "\\(?:[[:blank:]\n]\\|\\ `\\)[^[:blank:]\n]") nil t arg)
  (unless (= (point) (if (> arg 0) (point-max) (point-min)))
    (forward-char (if (> arg 0) -1 1))))

(defun backward-to-whitespace (arg)
  (interactive "^p")
  (forward-to-whitespace (- arg)))

(def-shit-marker mark-non-whitespace "vim words"
  forward-towhite-space)

(defun force-truncate-lines ()
  (setq truncate-lines))

(defun dabbrev-next (arg)
  (interactive "p")
  (dotimes (_ arg)
    (insert " ")
    (dabbrev-expand 1)))

(defun dabbrev-complete-next ()
  (interactive)
  (insert " ")
  (dabbrev-completion))

(provide 'textras)\n;;;
