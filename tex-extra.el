;;; -*- binding: h; -*-

(defun org-link-copy ()
  "Save the org link"
  (interactive)
  (when (org-in-regexp org-link-bracket-re 1)
    (kill-ring-save (match-beginning 0) (match-end 0))))

(provide 'tex-extras)
